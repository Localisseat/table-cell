obj={
  a0:{aa:[3,9], bb:2, cc:{aaa:4,bbb:-5}},
  a1:{aa:[0,8], bb:-7, cc:{aaa:8,bbb:7}},
  a2:{aa:[9,-4], bb:1, cc:{aaa:-1,bbb:8}},
  a3:{aa:[8,-1], bb:7, cc:{aaa:3,bbb:0}},
  a4:{aa:[-4,-2], bb:-2, cc:{aaa:8,bbb:9}}
};

function createTableCell() {
  let html = '';
  let minValueObj = 0;
  let maxValueObj = 0;
    for (let key in obj) {
      html = `
      <div class="row">
        <div class="col">${obj[key].aa[0] + obj[key].aa[1]}</div>
        <div class="col">${obj[key].bb}</div>
        <div class="col">${obj[key].cc.aaa + obj[key].cc.bbb}</div>
      </div>`;
      document.querySelector('.wrapper').insertAdjacentHTML('beforeend', html);

      minValueObj = Math.min(obj[key].aa[0], obj[key].aa[1], obj[key].bb, obj[key].cc.aaa, obj[key].cc.bbb, minValueObj);
      maxValueObj = Math.max(obj[key].aa[0], obj[key].aa[1], obj[key].bb, obj[key].cc.aaa, obj[key].cc.bbb, maxValueObj);

    }
    console.log('MaxValueObj: ', maxValueObj, ' MinValueObj: ', minValueObj);
  document.querySelector('.filling').disabled = 'true';
  }
